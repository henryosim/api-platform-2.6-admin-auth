import Head from "next/head";
import Link from "next/link";

const Welcome = () => (
    <>
        <Head>
            <title>Welcome to PrayAVerse.com!</title>
        </Head>

        <div className="welcome">
            <h1>Welcome to Pray a Verse</h1>
        </div>
    </>
);
export default Welcome;
